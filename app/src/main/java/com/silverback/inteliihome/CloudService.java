package com.silverback.inteliihome;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Created by office on 4/5/2017.
 */
public class CloudService extends Service implements MqttCallback,IMqttActionListener {
    private String clientId,type="cloud";
    public static final String topic = "test";
    IMqttToken token;
    private MqttAndroidClient client;
    MqttConnectOptions options;
    final static String MY_ACTION = "MY_ACTION";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();

        options = new MqttConnectOptions();
        options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
        clientId = MqttClient.generateClientId();


        }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        client =new MqttAndroidClient(this.getApplicationContext(), "tcp://35.154.217.161:1883", clientId);

        try {
            token=client.connect(options);
            token.setActionCallback(CloudService.this);


        } catch (MqttException e) {
            e.printStackTrace();
        }
        return START_STICKY;
    }

    @Override
    public void onSuccess(IMqttToken iMqttToken) {

        switch(type){
            case "cloud":
                Toast.makeText(this, "onSuccess", Toast.LENGTH_SHORT).show();
                Log.d("onSuccess", "onSuccess");
                Subscribed();
            break;
            case "subscribe":
                Log.d("Subscribed", "subscribe");
                Intent intent = new Intent();
                intent.setAction(MY_ACTION);

                intent.putExtra("DATAPASSED",1);

                sendBroadcast(intent);
                stopSelf();
                break;
        }

    }

    @Override
    public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
        Log.e("Fail", "Fail");
    }
    public void Subscribed(){
        try {
            type="subscribe";
            int qos = 1;
            token = client.subscribe(topic, qos);
            token.setActionCallback(this);
            Toast.makeText(this, "Successfully subscribed" + topic, Toast.LENGTH_SHORT).show();
        } catch (MqttException e) {

        }
    }

    @Override
    public void connectionLost(Throwable throwable) {

    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }
}

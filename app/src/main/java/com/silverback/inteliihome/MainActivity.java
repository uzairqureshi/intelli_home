package com.silverback.inteliihome;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.design.widget.TextInputLayout;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.silverback.inteliihome.CustomDrawer.TransDialog;

import ng.max.slideview.SlideView;

//Main Activity
public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText inputEmail, inputPassword;
    private TextView toolbar_text;
    private TextInputLayout inputLayoutEmail, inputLayoutPassword;
    private Button btnSignUp;
    private View snackbar;
    private Toolbar toolbar;
    private MyDialog progressDialog;
    MyReceiver myReceiver;
    SlideView slide;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);
        snackbar=(View)findViewById(R.id.snackbar_view);
        slide=(SlideView)findViewById(R.id.slider3);
        inputEmail = (EditText) findViewById(R.id.input_email);
        inputPassword = (EditText) findViewById(R.id.input_password);


        toolbar_text=(TextView)findViewById(R.id.title_toolbar);
        Intent myIntent = new Intent(this, CloudService.class);
        startService(myIntent);
        setToolbar();

        slide.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
//                progressDialog = new MyDialog(MainActivity.this);
//                progressDialog.setCancelable(true);
//                progressDialog.show();
            Intent i=new Intent(getApplicationContext(),Signup.class);
                startActivity(i);
            }
        });
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    @Override
    protected void onStart() {
        // TODO Auto-generated method stub

        //Register BroadcastReceiver
        //to receive event from our service
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CloudService.MY_ACTION);
        registerReceiver(myReceiver, intentFilter);

        Bundle extras = getIntent().getExtras();
        //Start our own service
        Intent intent = new Intent(MainActivity.this,CloudService.class);
        startService(intent);

        super.onStart();
    }

    @Override
    public void onClick(View v) {
//        switch(v.getId()){
//            case R.id.btn_signup:
//            btnSignUp.clearAnimation();
//            Intent i=new Intent(getApplicationContext(),Home.class);
//                startActivity(i);
////                progressDialog = new ProgressDialog(MainActivity.this, R.style.AppTheme_Dark_Dialog);
////                progressDialog.setMessage("Please Wait");
////
////                progressDialog.setIndeterminate(true);
////                progressDialog.setCancelable(true);
////
////
////                progressDialog.show();
////                Snackbar.make(snackbar,"Form Submit",Snackbar.LENGTH_SHORT).show();
//
//        }
    }

    private void setToolbar(){



    }
}
